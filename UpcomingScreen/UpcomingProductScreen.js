import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  ScrollView,
  TextInput,
  StyleSheet,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import SearchProducts from "./UpcomingProductSearch";
import UpcomingProductList from "./UpcomingProductList";

const Udata = require("../assets/data/Upcoming.json");

const UpcomingProductContainer = (props) => {
  const data = Udata;
  const [products, setProducts] = useState([]);
  const [productsFiltered, setProductsFiltered] = useState([]);
  const [focus, setFocus] = useState();

  useEffect(() => {
    setProducts(data);
    setProductsFiltered(data);

    return () => {
      setProducts([]);
      setProductsFiltered([]);
      setFocus();
    };
  }, []);
  const searchProduct = (text) => {
    setProductsFiltered(
      products.filter((i) => i.name.toLowerCase().includes(text.toLowerCase()))
    );
  };
  const openList = () => {
    setFocus(true);
  };
  const onBlur = () => {
    setFocus(false);
  };

  return (
    <ScrollView>
      <View style={styles.Searched}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            borderRadius: 10,
            backgroundColor: "#ccc",
            padding: 8,
          }}
        >
          <Icon name="search" size={20} style={{ marginLeft: 10 }} />
          <TextInput
            style={{ flex: 1, marginLeft: 10 }}
            placeholder="Search"
            onFocus={openList}
            onChangeText={(text) => searchProduct(text)}
          />
          {focus == true ? (
            <Icon size={20} onPress={onBlur} name="close" />
          ) : null}
        </View>
      </View>
      {focus == true ? (
        <SearchProducts
          navigation={props.navigation}
          productsFiltered={productsFiltered}
        />
      ) : (
        <View style={{ marginTop: 1 }}>
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <UpcomingProductList
                navigation={props.navigation}
                key={item.id}
                item={item}
              />
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      )}
    </ScrollView>
  );
};
export default UpcomingProductContainer;

const styles = StyleSheet.create({
  Searched: {
    margin: 12,
  },
});
