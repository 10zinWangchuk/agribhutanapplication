import React from "react";
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, Pressable } from "react-native";

var { width } = Dimensions.get("window");
const SearchProducts = (props) => {
  const { productsFiltered } = props;
  return (
    <View style={{ width: width }}>
      {productsFiltered.length > 0 ? (
        productsFiltered.map((item) => (
          <Pressable
            onPress={() => {
              props.navigation.navigate("UpcomingDetails", { item: item });
            }}
            key={item._id}
            style={styles.listItem}
          >
            <View style={styles.left}>
              <Image
                source={{
                  uri: item.image
                    ? item.image
                    : "https://cdn.pixabay.com/photo/2016/12/05/10/07/dish-1883501_1280.png",
                }}
                style={styles.thumbnail}
              />
            </View>
            <View style={styles.body}>
              <Text>{item.name}</Text>
              <Text style={styles.note}>{item.description}</Text>
            </View>
          </Pressable>
        ))
      ) : (
        <View style={styles.center}>
          <Text style={styles.centerText}>
            No products match the selected criteria
          </Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  listItem: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
  },
  left: {
    marginRight: 10,
  },
  thumbnail: {
    width: 50,
    height: 50,
  },
  body: {
    flex: 1,
  },
  note: {
    fontSize: 12,
    color: "gray",
  },
  center: {
    justifyContent: "center",
    alignItems: "center",
  },
  centerText: {
    alignSelf: "center",
  },
});

export default SearchProducts;
