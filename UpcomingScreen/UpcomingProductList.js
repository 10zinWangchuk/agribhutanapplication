
import React from "react";
import { TouchableOpacity, View, Dimensions, StyleSheet } from "react-native";
import UpcomingProductCard from "./UpcomingProductCard";

const UpcomingProductList = (props) => {
  const { item } = props;
  return (
    <TouchableOpacity
      style={styles.Container}
      onPress={() =>
        props.navigation.navigate("UpcomingDetails", { item: item })
      }
    >
      <View style={styles.innerContainer}>
        <UpcomingProductCard {...item} />
      </View>
    </TouchableOpacity>
  );
};
export default UpcomingProductList;
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    width: "100%",
  },
  innerContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#ccc",
  },
});
