import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import Icon from "react-native-vector-icons/FontAwesome";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { SafeAreaView } from "react-native-safe-area-context";

const SignIn = ({ navigation }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [number, setNumber] = useState("");
  const [password, setPassword] = useState("");
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const validateEmail = (email) => {
    // Use a regular expression to validate the email format
    const emailRegex = /\S+@\S+\.\S+/;
    return emailRegex.test(email);
  };

  const validateContact = (contact) => {
    // Validate the length of the contact number
    return contact.length === 8;
  };

  const handleSignup = () => {
    if (!validateEmail(email)) {
      setErrorMessage("Invalid email format");
      return;
    }

    if (!validateContact(number)) {
      setErrorMessage("Contact number must be 8 digits");
      return;
    }

    if (password !== confirmPassword) {
      setErrorMessage("Passwords don't match");
      return;
    };
    navigation.navigate("Login");
  };

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  return (
    <ScrollView>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        keyboardShouldPersistTaps="handled"
      >
        <View style={styles.content}>
          <Image
            source={require("../assets/image/logo.jpg")}
            style={styles.logo}
          />
          <Text style={styles.title}>SignUp</Text>
          {errorMessage !== "" && (
            <Text style={styles.error}>{errorMessage}</Text>
          )}
          <View style={styles.input}>
            <Ionicons name="ios-person-outline" size={24} color="#808080" />
            <TextInput
              style={styles.inputText}
              placeholder="Name"
              onChangeText={setName}
              value={name}
            />
          </View>
          <View style={styles.input}>
            <MaterialIcons name="attach-email" size={24} color="#808080" />
            <TextInput
              style={styles.inputText}
              placeholder="Email"
              onChangeText={setEmail}
              value={email}
            />
          </View>
          <View style={styles.input}>
            <MaterialCommunityIcons
              name="phone-plus"
              size={24}
              color="#808080"
            />
            <TextInput
              style={styles.inputText}
              placeholder="Contact"
              onChangeText={setNumber}
              value={number}
            />
          </View>
          <View style={styles.input}>
            <MaterialIcons name="lock-outline" size={24} color="#808080" />
            <TextInput
              style={styles.inputText}
              placeholder="Password"
              onChangeText={setPassword}
              value={password}
              secureTextEntry={!isPasswordVisible}
            />
            <TouchableOpacity onPress={togglePasswordVisibility}>
              <Icon
                name={isPasswordVisible ? "eye-slash" : "eye"}
                size={20}
                color="#999"
                style={styles.eyeIcon}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.input}>
            <MaterialIcons name="lock-outline" size={24} color="#808080" />
            <TextInput
              style={styles.inputText}
              placeholder="Confirm Password"
              onChangeText={setConfirmPassword}
              value={confirmPassword}
              secureTextEntry={!isPasswordVisible}
            />
            <TouchableOpacity onPress={togglePasswordVisibility}>
              <Icon
                name={isPasswordVisible ? "eye-slash" : "eye"}
                size={20}
                color="#999"
                style={styles.eyeIcon2}
              />
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={styles.button} onPress={handleSignup}>
            <Text style={styles.buttonText}>Sign Up</Text>
          </TouchableOpacity>
          <View style={styles.row}>
            <Text style={{ color: "#808080" }}>Already have an account?</Text>
            <TouchableOpacity onPress={() => navigation.navigate("Login")}>
              <Text style={styles.link}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
  },
  content: {
    width: "80%",
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 160,
    height: 160,
    resizeMode: "contain",
    marginBottom: 10,
    borderRadius: 80,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 30,
  },
  input: {
    width: "100%",
    height: 50,
    borderWidth: 1,
    borderColor: "#D9D9D9",
    borderRadius: 5,
    paddingLeft: 10,
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  inputText: {
    flex: 1,
    marginLeft: 10,
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: "#26AE60",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#fff",
    fontSize: 15,
  },
  row: {
    flexDirection: "row",
    marginTop: 20,
  },
  link: {
    color: "#26AE60",
    marginLeft: 5,
  },
  eyeIcon: {
    marginLeft: 150,
    marginRight: 10,
  },
  eyeIcon2: {
    marginLeft: 90,
    marginRight: 10,
  },
  error: {
    color: "red",
    marginBottom: 10,
  },
});

export default SignIn;
