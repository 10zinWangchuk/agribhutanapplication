import React, { useState } from 'react';
import { View, TextInput, Button, Image, StyleSheet } from 'react-native';
import ImagePicker from 'react-native-image-picker';

const PostScreen = () => {
  const [postText, setPostText] = useState('');
  const [postImage, setPostImage] = useState(null);

  const handleChooseImage = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error:', response.error);
      } else {
        const source = { uri: response.uri };
        setPostImage(source);
      }
    });
  };

  const handlePost = () => {
    // Post logic goes here
    console.log('Posting...');
    console.log('Text:', postText);
    console.log('Image:', postImage);
    // Reset form after posting
    setPostText('');
    setPostImage(null);
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Write your post"
          value={postText}
          onChangeText={setPostText}
          multiline={true}
        />
        <Button title="Choose Image" onPress={handleChooseImage} />
      </View>
      {postImage && (
        <View style={styles.imageContainer}>
          <Image source={postImage} style={styles.image} />
        </View>
      )}
      <Button title="Post" onPress={handlePost} disabled={!postText} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
  },
  inputContainer: {
    marginBottom: 12,
  },
  input: {
    height: 100,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 12,
    paddingHorizontal: 8,
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 12,
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'cover',
  },
});

export default PostScreen;
