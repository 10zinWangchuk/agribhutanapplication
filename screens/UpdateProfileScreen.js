import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { IconButton } from "@react-native-material/core";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import * as ImagePicker from "expo-image-picker";
import { MaterialIcons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";

function UpdateProfileScreen() {
  const navigation = useNavigation();
  const def = require("../assets/image/R.jpeg");
  const [image, setImage] = useState(null);

  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.canceled) {
      setImage(result.assets[0].uri);
    }
  };
  const handleBack = () => {
    navigation.navigate("Profile");
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.profileAndLogout}>
        <TouchableOpacity onPress={handleBack}>
          <Ionicons
            style={styles.backIcon}
            name="arrow-back"
            size={25}
            color="white"
          />
        </TouchableOpacity>
        <View style={styles.heading1}>
          <Text style={styles.heading}>Update Your Profile</Text>
        </View>
      </View>
      <View style={styles.innerContainer}>
        <View style={styles.fields}>
          <View style={styles.avatarContainer}>
            <Image style={styles.img} source={{ uri: image }} />
            <IconButton
              style={styles.changeButton}
              icon={(props) => <Icon name="camera-outline" {...props} />}
              onPress={pickImage}
            />
          </View>
          <View style={styles.inputContainer}>
            <AntDesign name="user" size={24} />
            <TextInput style={styles.input} placeholder="Username" />
          </View>
          <View style={styles.inputContainer}>
            <MaterialIcons name="mail-outline" size={24} />
            <TextInput style={styles.input} placeholder="Email" />
          </View>
          <View style={styles.inputContainer}>
            <MaterialCommunityIcons name="phone-plus-outline" size={24} />
            <TextInput style={styles.input} placeholder="Contact Number" />
          </View>

          <View style={styles.buttonStyleContainer}>
            <Button title="Update" style={styles.button} color="#2DB064" />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default UpdateProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2DB064",
    width: "100%",
  },
  innerContainer: {
    width: "100%",
    backgroundColor: "white",
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexGrow: 1,
  },

  fields: {
    marginBottom: "10%",
  },
  logout: {
    marginLeft: "auto",
    marginTop: "5%",
    marginRight: 20,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderColor: "#ccc",
    borderRadius: 5,
    paddingHorizontal: 10,
    marginVertical: 10,
    width: "80%",
  },
  input: {
    flex: 1,
    height: 40,
    fontSize: 14,
    paddingHorizontal: 20,
  },
  avatarContainer: {
    alignItems: "center",
  },
  img: {
    width: "50%",
    aspectRatio: 1,
    borderRadius: 70,
    marginBottom: "5%",
    borderWidth: 2,
    borderColor: "#ccc",
  },
  changeButton: {
    borderRadius: 30,
    padding: 1,
    marginTop: -45,
    marginLeft: 50,
  },
  buttonStyleContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: "10%",
    marginTop: 20,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: "#2DB064",
  },
  profileAndLogout: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    marginBottom: "5%",
    marginTop: "5%",
    marginRight: "18%",
  },
  heading: {
    fontSize: 20,
    color: "white",
  },
  heading1: {},
  backIcon: {
    marginRight: "16%",
  },
});
