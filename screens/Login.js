import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import Icon from "react-native-vector-icons/FontAwesome";
import { useNavigation } from "@react-navigation/native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const Login = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleLogin = () => {
    // Perform email validation before navigating to HomeBottom
    if (!validateEmail(email)) {
      setErrorMessage("Please enter a valid email address");
      return;
    }

    navigation.navigate("HomeBottom");
  };

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const handleGoToSignIn = () => {
    navigation.navigate("SignIn");
  };

  const validateEmail = (email) => {
    // Simple email validation using regular expression
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        contentContainerStyle={styles.scrollViewContentContainer}
        keyboardShouldPersistTaps="handled"
      >
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          style={styles.keyboardAvoidingView}
        >
          <View style={styles.innerContainer}>
            <Image
              style={styles.logo}
              source={require("../assets/image/logo.jpg")}
            />
            <Text style={styles.title}>Login</Text>
            <View style={styles.input}>
              <MaterialIcons name="attach-email" size={24} color="#808080" />
              <TextInput
                style={styles.inputText}
                placeholder="Email"
                onChangeText={setEmail}
                value={email}
              />
            </View>
            {errorMessage !== "" && (
              <Text style={styles.error}>{errorMessage}</Text>
            )}
            <View style={styles.input}>
              <MaterialIcons name="lock-outline" size={24} color="#808080" />
              <TextInput
                style={styles.inputText}
                placeholder="Password"
                onChangeText={setPassword}
                value={password}
                secureTextEntry={!isPasswordVisible}
              />
              <TouchableOpacity onPress={togglePasswordVisibility}>
                <Icon
                  name={isPasswordVisible ? "eye-slash" : "eye"}
                  size={20}
                  color="#999"
                  style={styles.eyeIcon}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.button} onPress={handleLogin}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <View style={styles.row}>
              <Text style={{ color: "#808080" }}>New here? </Text>
              <TouchableOpacity onPress={handleGoToSignIn}>
                <Text style={styles.link}>SignUp</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  scrollViewContentContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingBottom: 40,
  },
  keyboardAvoidingView: {
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  innerContainer: {
    width: "100%",
    maxWidth: 400,
    alignItems: "center",
  },
  inputText: {
    flex: 1,
    paddingLeft: 10,
  },
  row: {
    flexDirection: "row",
    marginTop: 20,
    justifyContent: "center",
  },
  logo: {
    width: 160,
    height: 160,
    resizeMode: "contain",
    marginBottom: 20,
    borderRadius: 80,
    marginTop: '5%'
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 30,
    textAlign: "center",
  },
  input: {
    width: "100%",
    height: 50,
    borderWidth: 1,
    borderColor: "#D9D9D9",
    borderRadius: 5,
    paddingLeft: 10,
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: "#2AB063",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
  },
  buttonText: {
    color: "#fff",
    fontSize: 15,
  },
  eyeIcon: {
    marginLeft: 10,
    marginRight: 10,
  },
  link: {
    color: "#2AB063",
    fontWeight: "bold",
  },
  error: {
    color: "red",
    marginBottom: 10,
  },
});

export default Login;
