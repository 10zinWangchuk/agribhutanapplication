import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useNavigation } from "@react-navigation/native";

const AboutUsScreen = () => {
  const navigation = useNavigation();

  const handleBack = () => {
    navigation.navigate("Profile");
  };

  return (
    <SafeAreaView style={styles.root}>
      <TouchableOpacity onPress={handleBack}>
        <Ionicons
          style={styles.backIcon}
          name="arrow-back"
          size={25}
          color="white"
        />
      </TouchableOpacity>
      <ScrollView style={styles.container}>
        {/* <Text style={styles.title}>About Us</Text> */}
        <Text style={styles.title}>
          {" "}
          Welcome to our AgriBhutan application!{" "}
        </Text>
        <Text style={styles.description}>
          {" "}
          AgriBhutan application is mainly to the people of our country Bhutan
          who is dealing the agricultural products and the materials. AgriBhutan
          is an application to connect the Farmers directly to the customers as
          well the shopkeepers who are dealing with the farming materials to the
          farmers.
        </Text>
        <Text style={styles.description}>
          Here you will find a powerful set of features that make your life
          easier.
        </Text>

        <Text style={styles.subtitle}>Features:</Text>

        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 1: Posting the farming products.
          </Text>
        </View>

        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 2: Posting the farming materials.
          </Text>
        </View>

        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 3: Posting the upcoming products
          </Text>
        </View>
        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 3: Reporting to the particular post.
          </Text>
        </View>
        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 3: Giving feedback to the application.
          </Text>
        </View>
        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 3: Searching the post made by others. 
          </Text>
        </View>
       
        <View style={styles.featureContainer}>
          <Ionicons name="md-checkmark-circle" size={20} color="#2DB064" />
          <Text style={styles.featureText}>
            Feature 3:You can update your own profile and password.
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = {
  root: {
    flex: 1,
    backgroundColor: "#2DB064",
    width: "100%",
  },
  container: {
    backgroundColor: "white",
    marginTop: "4%",
    flex: 1,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    padding: "8%",
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: "5%",
    marginTop: "6%",
  },
  description: {
    marginBottom: "5%",
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: "3%",
  },
  featureContainer: {
    flexDirection: "row",
    marginBottom: "2%",
    marginRight: "2%",
  },
  featureText: {
    marginLeft: "2%",
  },
  backIcon: {
    marginLeft: "8%",
    marginTop: "4%",
  },
};

export default AboutUsScreen;
