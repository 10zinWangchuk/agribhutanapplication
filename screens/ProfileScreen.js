import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  TextInput,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import * as ImagePicker from "expo-image-picker";
import { MaterialIcons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";
import Ionicons from "react-native-vector-icons/Ionicons";
import UpdateProfileScreen from "./UpdateProfileScreen";

function ProfileScreen() {
  const navigation = useNavigation();
  const def = require("../assets/image/logo.jpg");
  const [image, setImage] = useState(null);

  const handleSignup = () => {
    navigation.navigate("Login");
  };
  const UpdateProfile = () => {
    navigation.navigate("UpdateProfile");
  };
  const PasswordUpdateHandler = () => {
    navigation.navigate("PasswordUpdate");
  };
  const FeedBackUpdateHandler = () => {
    navigation.navigate("FeedBack");
  };
  const AboutUsHandler = () => {
    navigation.navigate("AboutUs");
  };
  
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.profileAndLogout}>
        <View style={styles.heading1}>
          <Text style={styles.heading}>My Profile</Text>
        </View>
        <TouchableOpacity style={styles.logout} onPress={handleSignup}>
          <MaterialIcons name="logout" size={24} color="white" />
        </TouchableOpacity>
      </View>
      <View style={styles.innerContainer}>
        <View style={styles.fields}>
          <View style={styles.avatarContainer}>
            <View style={styles.img}>
              <Image />
            </View>

            <View style={styles.Username}>
              <Text style={styles.ProfileName}>Tenzin wangchuk</Text>
            </View>
            <View style={styles.Username}>
              <MaterialIcons name="mail-outline" size={20} marginRight={8} />
              <Text>201.00340.11.0100@education.gov.bt</Text>
            </View>
          </View>

          <View style={styles.ViewContainer}>
            <TouchableOpacity
              style={styles.pressableComponent}
              onPress={UpdateProfile}
            >
              <View style={styles.Ionicons}>
                <Ionicons name="settings-outline" size={24} color="black" />
              </View>
              <View>
                <Text style={styles.TextInsideContainer}>Profile Setting</Text>
              </View>
              <TouchableOpacity style={styles.ForwardIcon} onPress={UpdateProfile}>
                <Ionicons name="arrow-forward" size={24} color="black" />
              </TouchableOpacity>
            </TouchableOpacity>
            <TouchableOpacity style={styles.pressableComponent} onPress={PasswordUpdateHandler}>
              <View style={styles.Ionicons}>
                <Ionicons
                  name="ios-lock-closed-outline"
                  size={24}
                  color="black"
                />
              </View>
              <View>
                <Text style={styles.TextInsideContainer}>Password Setting</Text>
              </View>
              <TouchableOpacity style={styles.ForwardIcon}onPress={PasswordUpdateHandler} >
                <Ionicons name="arrow-forward" size={24} color="black" />
              </TouchableOpacity>
            </TouchableOpacity>
            <TouchableOpacity style={styles.pressableComponent} onPress={FeedBackUpdateHandler}>
              <View style={styles.Ionicons}>
                <Ionicons
                  name="chatbubble-ellipses-outline"
                  size={24}
                  color="black"
                />
              </View>
              <View>
                <Text style={styles.TextInsideContainer}>Feed Back</Text>
              </View>
              <TouchableOpacity style={styles.ForwardIcon} onPress={FeedBackUpdateHandler}>
                <Ionicons name="arrow-forward" size={24} color="black" />
              </TouchableOpacity>
            </TouchableOpacity>

            <TouchableOpacity style={styles.pressableComponent} onPress={AboutUsHandler}>
              <View style={styles.Ionicons}>
                <Ionicons
                  name="information-circle-outline"
                  size={24}
                  color="black"
                />
              </View>
              <View>
                <Text style={styles.TextInsideContainer}>About Us</Text>
              </View>
              <TouchableOpacity style={styles.ForwardIcon}>
                <Ionicons name="arrow-forward" size={24} color="black" />
              </TouchableOpacity>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2DB064",
    width: "100%",
  },
  innerContainer: {
    width: "100%",
    backgroundColor: "white",
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    flexGrow: 1,
  },

  fields: {
    marginBottom: "20%",
  },
  logout: {
    marginLeft: "auto",
    marginTop: "5%",
    marginRight: "7%",
  },

  avatarContainer: {
    alignItems: "center",
    backgroundColor: "#cccc",
    borderRadius: 20,
    marginTop: "40%",
  },
  img: {
    width: "50%",
    aspectRatio: 1,
    borderRadius: 80,
    borderWidth: 2,
    borderColor: "#ccc",
    marginTop: -90,
    backgroundColor: '#2DB064',
  },
  profileAndLogout: {
    flexDirection: "row",
    alignItems: "flex-end",
    marginBottom: "5%",
  },
  heading: {
    fontSize: 20,
    color: "white",
  },
  heading1: {
    marginRight: "50%",
    marginLeft: "10%",
  },
  Username: {
    flexDirection: "row",
  },
  ProfileName: {
    fontWeight: "bold",
    fontSize: 18,
  },
  ViewContainer: {
    backgroundColor: "#cccc",
    marginVertical: "8%",
    width: "100%",
    borderRadius: 20,
  },
  pressableComponent: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: 15,
  },
  Ionicons: {
    marginRight: "4%",
    marginLeft: "4%",
  },
  ForwardIcon: {
    marginLeft: "auto",
  },
  TextInsideContainer: {
    marginRight: "35%",
    fontSize: 14,
    fontWeight: 'bold'
  },

});
