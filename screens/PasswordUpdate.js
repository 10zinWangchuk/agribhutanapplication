import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import {
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useNavigation } from "@react-navigation/native";

const PasswordChangeForm = () => {
  const navigation = useNavigation();

  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handlePasswordChange = () => {
    if (!oldPassword || !newPassword || !confirmPassword) {
      // Check if any of the fields are empty
      console.log("Please fill in all the fields");
      return;
    }

    if (newPassword !== confirmPassword) {
      // Check if the new password and confirm password match
      console.log("New password and confirm password do not match");
      return;
    }

    // Perform the password change API call or logic here
    // You can use the values from the state variables (oldPassword, newPassword, confirmPassword) to make the necessary changes

    // Example API call using fetch:
    fetch("https://example.com/api/password/change", {
      method: "POST",
      body: JSON.stringify({
        oldPassword,
        newPassword,
      }),
      headers: {
        "Content-Type": "application/json",
        // Add any necessary headers
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Password change successful");
        // Perform any necessary actions after successful password change
      })
      .catch((error) => {
        console.log("Error changing password:", error);
        // Handle the error appropriately
      });
  };

  const handleBack = () => {
    navigation.navigate("Profile");
  };

  return (
    <SafeAreaView style={styles.root}>
      <TouchableOpacity onPress={handleBack}>
        <Ionicons
          style={styles.backIcon}
          name="arrow-back"
          size={25}
          color="white"
        />
      </TouchableOpacity>
      <ScrollView style={styles.formContainer}>
        <Text style={styles.title}>Change Password</Text>

        <TextInput
          style={styles.input}
          placeholder="Old Password"
          value={oldPassword}
          onChangeText={setOldPassword}
          secureTextEntry
        />
        
        <TextInput
          style={styles.input}
          placeholder="New Password"
          value={newPassword}
          onChangeText={setNewPassword}
          secureTextEntry
        />

        <TextInput
          style={styles.input}
          placeholder="Confirm Password"
          value={confirmPassword}
          onChangeText={setConfirmPassword}
          secureTextEntry
        />

        <TouchableOpacity
          style={styles.submitButton}
          onPress={handlePasswordChange}
        >
          <Text style={styles.submitButtonText}>Change Password</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = {
  root: {
    flex: 1,
    backgroundColor: "#2DB064",
    width: "100%",
  },
  formContainer: {
    backgroundColor: "white",
    marginTop: "4%",
    flex: 1,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  input: {
    borderWidth: 2,
    borderRadius: 8,
    borderColor: "#ccc",
    padding: 8,
    marginBottom: "5%",
    marginHorizontal: "8%",
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: "7%",
    marginTop: "6%",
  },
  backIcon: {
    marginLeft: "8%",
    marginTop: "4%",
  },
  submitButton: {
    backgroundColor: "#2DB064",
    padding: 10,
    alignItems: "center",
    marginHorizontal: "30%",
    borderRadius: 12,
  },
  submitButtonText: {
    color: "white",
    fontWeight: "bold",
  },
};

export default PasswordChangeForm;
