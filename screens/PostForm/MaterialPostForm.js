import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import {
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  ScrollView,
  Image,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { IconButton } from "@react-native-material/core";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { SafeAreaView } from "react-native-safe-area-context";
import { useNavigation } from "@react-navigation/native";

const MaterialPostForm = () => {
  const navigation = useNavigation();
  const [image, setImage] = useState(null);
  const [name, setName] = useState("");
  const [pricePerKg, setPricePerKg] = useState("");
  const [location, setLocation] = useState("");
  const [quantity, setQuantity] = useState("");
  const [contact, setContact] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");

  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.canceled) {
      setImage(result.assets[0].uri);
    }
  };

  const handlePost = () => {
    // Handle post logic here
    // You can access the form data in the respective state variables (name, pricePerKg, location, etc.)
  };
  const Backbutton = () => {
    navigation.navigate("Post");
  };

  return (
    <SafeAreaView style={styles.root}>
      <TouchableOpacity onPress={Backbutton}>
        <Ionicons style={styles.BackIcon} name="arrow-back" size={25} />
      </TouchableOpacity>
      <ScrollView style={styles.formContainer}>
        <Text style={styles.title}>Material Post Form</Text>
        <View style={styles.avatarContainer}>
          <Image style={styles.image} source={{ uri: image }} />
          <IconButton
            style={styles.changeButton}
            icon={() => (
              <Icon name="camera" size={30} style={styles.editIcon} />
            )}
            onPress={pickImage}
          />
        </View>
        <TextInput
          style={styles.input}
          placeholder="Name"
          value={name}
          onChangeText={setName}
        />

        <TextInput
          style={styles.input}
          placeholder="Price"
          value={pricePerKg}
          onChangeText={setPricePerKg}
        />

        <TextInput
          style={styles.input}
          placeholder="Location"
          value={location}
          onChangeText={setLocation}
        />

        <TextInput
          style={styles.input}
          placeholder="Quantity"
          value={quantity}
          onChangeText={setQuantity}
        />

        <TextInput
          style={styles.input}
          placeholder="Contact"
          value={contact}
          onChangeText={setContact}
          keyboardType="numeric"
        />
        <TextInput
          style={styles.input}
          placeholder="Date of available (DD-MM-YYYY)"
          value={date}
          onChangeText={setDate}
          keyboardType="numeric"
        />

        <TextInput
          style={styles.description}
          placeholder="Description"
          value={description}
          onChangeText={setDescription}
          multiline
        />

        <TouchableOpacity style={styles.Postbutton} onPress={handlePost}>
          <Text>Post</Text>
        </TouchableOpacity>

        {/* <BottomTab/> */}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = {
  root: {
    flex: 1,
    backgroundColor: "#2DB064",
    width: "100%",
  },
  formContainer: {
    backgroundColor: "white",
    marginTop: "4%",
    flex: 1,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  input: {
    borderWidth: 2,
    borderColor: "#ccc",
    padding: 8,
    marginBottom: "5%",
    marginHorizontal: "8%",
    borderRadius: 10,
  },
  description: {
    borderWidth: 2,
    borderColor: "#ccc",
    padding: 20,
    marginBottom: "5%",
    marginHorizontal: "8%",
    borderRadius: 12,
  },

  fileInput: {
    borderWidth: 2,
    borderColor: "#ccc",
    padding: 8,
    marginBottom: "5%",
    marginHorizontal: "8%",
  },

  title: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: "7%",
    marginTop: "6%",
  },
  Postbutton: {
    backgroundColor: "#2DB064",
    padding: 10,
    alignItems: "center",
    marginHorizontal: "30%",
    borderRadius: 12,
    marginBottom: "5%",
  },
  BackIcon: {
    marginLeft: "8%",
    marginTop: "4%",
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: "gray",
    marginTop: "3%",
  },
  avatarContainer: {
    alignItems: "center",
  },
  changeButton: {
    borderRadius: 30,
    padding: 1,
    marginTop: -26,
    marginLeft: "30%",
  },
  editIcon: {
    color: 'grey',
  },
};

export default MaterialPostForm;
