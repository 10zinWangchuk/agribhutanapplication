import React from "react";
import { StyleSheet, Text, Pressable, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

const PostComponentScreen = ({ navigation }) => {
  const closeModal = () => {
    navigation.navigate("Home");
  };
  const VegetableFormHandler = () => {
    navigation.navigate("VegetablePostForm");
  };
  const MaterialFormHandler = () => {
    navigation.navigate("MaterialPostForm");
  };
  const UpcomingFormHandler = () => {
    navigation.navigate("UpcomingForm");
  };
  const ReportFormHandler = () => {
    navigation.navigate('ReportForm');
  };

  return (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <Text style={styles.modalText}>What do you want to post?</Text>
        <View
          style={{ flexDirection: "column", justifyContent: "space-between" }}
        >
          <Pressable style={styles.option} onPress={VegetableFormHandler}>
            <Text style={styles.textStyle}>Vegetable/Fruits</Text>
          </Pressable>
          <Pressable style={styles.option} onPress={MaterialFormHandler}>
            <Text style={styles.textStyle}>Materials</Text>
          </Pressable>
          <Pressable style={styles.option} onPress={UpcomingFormHandler}>
            <Text style={styles.textStyle}>Upcoming Products</Text>
          </Pressable>
        </View>
        <Pressable style={styles.close} onPress={closeModal}>
          <Text style={styles.CloseText}>Close</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  close: {
    marginLeft: "50%",
    marginTop: 12,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "bold",
  },
  option: {
    borderRadius: 20,
    padding: 10,
    paddingHorizontal: "15%",
    elevation: 2,
    backgroundColor: "#2DB064",
    marginVertical: 6,
  },
  CloseText: {
    color: "blue",
    fontWeight: "bold",
    // backgroundColor: "#2DB064",
    padding: 10,
    borderRadius: 12,
  },
});

export default PostComponentScreen;
