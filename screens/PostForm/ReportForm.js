import React, { useState } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
  ScrollView,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import BottomTab from "../../component/Navigators/BottomTab";

const ReportForm = () => {
  const [selectedCategory, setSelectedCategory] = useState("");
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");

  const handleSubmit = () => {
    // Handle form submission here
  };

  const isIOS = Platform.OS === "ios";

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView
          style={styles.keyboardAvoidingContainer}
          behavior={isIOS ? "padding" : undefined}
        >
          <View style={styles.innerContainer}>
            <Text style={styles.heading}>Report Form</Text>
            <View style={styles.formContainer}>
              <Text style={styles.label}>Category</Text>
              <View style={styles.input}>
                <Picker
                  selectedValue={selectedCategory}
                  onValueChange={(itemValue) => setSelectedCategory(itemValue)}
                  style={{ height: 40 }}
                >
                  <Picker.Item label="Select a category" value="" />
                  <Picker.Item
                    label="Vegetables/Fruits"
                    value="Vegetables/Fruits"
                  />
                  <Picker.Item label="Materials" value="Materials" />
                  <Picker.Item
                    label="Upcoming Products"
                    value="Upcoming Products"
                  />
                </Picker>
              </View>
              <Text style={styles.label}>Description</Text>
              <View style={[styles.input, styles.descriptionInput]}>
                <TextInput
                  style={styles.description}
                  placeholder="Enter description"
                  onChangeText={setDescription}
                  value={description}
                  multiline
                  numberOfLines={5}
                  keyboardType={isIOS ? "default" : "email-address"}
                />
              </View>
              <Text style={styles.label}>Date</Text>
              <View style={styles.input}>
                <TextInput
                  style={{ height: 40 }}
                  placeholder="DD-MM-YYYY"
                  onChangeText={(text) => setDate(text)}
                  value={date}
                  keyboardType={isIOS ? "default" : "numeric"}
                />
              </View>
              <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
      {/* <BottomTab/> */}
    </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexGrow:1,
    backgroundColor: "#2DB064",
  },

  innerContainer: {
    flex: 1,
    flexGrow: 1,
    width: "100%",
    backgroundColor: "white",
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: "12%",
  },
  formContainer: {
    width: "90%",
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 10,
    padding: 12,
    margin: 11,
  },
  heading: {
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 20,
    marginTop: 20,
    textAlign: "center",
  },
  input: {
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 8,
    padding: "2%",
    marginBottom: "4%",
  },
  label: {
    fontSize: 20,
    marginBottom: 10,
  },
  descriptionInput: {
    height: "16%",
  },
  description: {
    textAlignVertical: "top",
  },
  button: {
    backgroundColor: "#2DB064",
    padding: 12,
    borderRadius: 10,
    marginVertical: 20,
    alignItems: "center",
  },
  buttonText: {
    color: "#fff",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
});

export default ReportForm;
