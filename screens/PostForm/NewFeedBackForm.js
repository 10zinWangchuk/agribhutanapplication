import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import {
  View,
  Text,
  TextInput,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { useNavigation } from "@react-navigation/native";

const FeedbackScreen = () => {
  const navigation = useNavigation();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [feedback, setFeedback] = useState("");

  const handleFeedbackSubmit = () => {
    if (!name || !email || !feedback) {
      console.log("Please fill in all the fields");
      return;
    }

    // Perform the feedback submission API call or logic here
    // You can use the values from the state variables (name, email, feedback) to submit the feedback

    // Example API call using fetch:
    fetch("https://example.com/api/feedback/submit", {
      method: "POST",
      body: JSON.stringify({
        name,
        email,
        feedback,
      }),
      headers: {
        "Content-Type": "application/json",
        // Add any necessary headers
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Feedback submitted successfully");
        // Perform any necessary actions after successful feedback submission
      })
      .catch((error) => {
        console.log("Error submitting feedback:", error);
        // Handle the error appropriately
      });
  };

  const handleBack = () => {
    navigation.navigate("Profile");
  };

  return (
    <SafeAreaView style={styles.root}>
      <TouchableOpacity onPress={handleBack}>
        <Ionicons
          style={styles.backIcon}
          name="arrow-back"
          size={25}
          color="white"
        />
      </TouchableOpacity>
      <ScrollView style={styles.formContainer}>
        <Text style={styles.title}>Feedback</Text>

        <TextInput
          style={styles.input}
          placeholder="Name"
          value={name}
          onChangeText={setName}
        />

        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />

        <TextInput
          style={styles.feedbackInput}
          placeholder="Feedback"
          value={feedback}
          onChangeText={setFeedback}
          multiline
        />

        <TouchableOpacity
          style={styles.submitButton}
          onPress={handleFeedbackSubmit}
        >
          <Text style={styles.submitButtonText}>Submit Feedback</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = {
  root: {
    flex: 1,
    backgroundColor: "#2DB064",
    width: "100%",
  },
  formContainer: {
    backgroundColor: "white",
    marginTop: "4%",
    flex: 1,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  input: {
    borderWidth: 2,
    borderColor: "#ccc",
    padding: 8,
    marginBottom: "5%",
    marginHorizontal: "8%",
    borderRadius: 8,
  },
  feedbackInput: {
    borderWidth: 2,
    borderRadius: 8,
    borderColor: "#ccc",
    padding: 8,
    marginBottom: "5%",
    marginHorizontal: "8%",
    height: 150, // Adjust the height as needed
    textAlignVertical: "top", // Start the text from the top of the input
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: "7%",
    marginTop: "6%",
  },
  backIcon: {
    marginLeft: "8%",
    marginTop: "4%",
  },
  submitButton: {
    backgroundColor: "#2DB064",
    padding: 10,
    alignItems: "center",
    marginHorizontal: "30%",
    borderRadius: 12,
  },
  submitButtonText: {
    color: "white",
    fontWeight: "bold",
  },
};

export default FeedbackScreen;
