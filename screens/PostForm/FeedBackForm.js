import React, { useState } from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
} from 'react-native';
import { Picker } from '@react-native-picker/picker';

const FeedbackForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [feedback, setFeedback] = useState('');

  const handleNameChange = (text) => {
    setName(text);
  };

  const handleEmailChange = (text) => {
    setEmail(text);
  };

  const handleFeedbackChange = (text) => {
    setFeedback(text);
  };

  const handleSubmit = () => {
  };

  const isIOS = Platform.OS === 'ios';

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        style={styles.keyboardAvoidingContainer}
        behavior={isIOS ? 'padding' : undefined}
      >
        <View style={styles.innerContainer}>
          <View>
             <Text style={styles.heading}>Feedback Form</Text>
          </View>
          <View style={styles.formContainer}> 
            <Text style={styles.label}>Name</Text>
            <View style={styles.input}>
              <TextInput
                  placeholder='Enter Name'
                  value={name}
                  onChangeText={handleNameChange}
                />
            </View>
              <Text style={styles.label}>Email</Text>
             <View style={styles.input}>
             <TextInput
              placeholder="Enter your Email"
              value={email}
              onChangeText={handleEmailChange}
            />
            </View> 
            <Text style={styles.label}>Feedback</Text>
             <View style={[styles.input, styles.feedbackInput]}>
              <TextInput
              style={styles.feedback}
              placeholder="Feedback"
              multiline={true}
              numberOfLines={5}
              value={feedback}
              onChangeText={handleFeedbackChange}
            />      
          </View>
           <TouchableOpacity style={styles.button} onPress={handleSubmit}>
              <Text style={styles.buttonText}>Submit</Text>
            </TouchableOpacity>
        </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2DB064',
  },
  keyboardAvoidingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainer: {
    width: width * 0.99,
    height: '90%',
    backgroundColor: 'white',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    width: '90%',
    height: '90%',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 10,
    padding: 30,
  },
  heading: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 8,
    padding: 15,
    marginBottom: 30,
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  label: {
    fontSize: 20,
  },
  feedbackInput: {
    height: 150,
  },
    feedback: {
    textAlignVertical: 'top',
  },
  button: {
    backgroundColor: '#2DB064',
    padding: 12,
    borderRadius: 10,
    marginLeft: 50,
    marginRight: 50,
    marginTop:20,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default FeedbackForm;
