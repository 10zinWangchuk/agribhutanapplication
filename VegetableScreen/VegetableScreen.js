import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  ScrollView,
  TextInput,
  StyleSheet,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import SearchVegetables from "./VegetableSearch";
import VegetableList from "./VegetableList";

const Vdata = require("../assets/data/Vegetable.json");

const VegetableContainer = (props) => {
  const data = Vdata;
  const [vegetables, setVegetables] = useState([]);
  const [productsFiltered, setProductsFiltered] = useState([]);
  const [focus, setFocus] = useState();

  useEffect(() => {
    setVegetables(data);
    setProductsFiltered(data);

    return () => {
      setVegetables([]);
      setProductsFiltered([]);
      setFocus();
    };
  }, []);
  const searchProduct = (text) => {
    setProductsFiltered(
      vegetables.filter((i) => i.name.toLowerCase().includes(text.toLowerCase()))
    );
  };
  const openList = () => {
    setFocus(true);
  };
  const onBlur = () => {
    setFocus(false);
  };

  return (
    <ScrollView>
      <View style={styles.Searched}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            borderRadius: 10,
            backgroundColor: "#ccc",
            padding: 8,
          }}
        >
          <Icon name="search" size={20} style={{ marginLeft: 10 }} />
          <TextInput
            style={{ flex: 1, marginLeft: 10 }}
            placeholder="Search"
            onFocus={openList}
            onChangeText={(text) => searchProduct(text)}
          />
          {focus == true ? (
            <Icon size={20} onPress={onBlur} name="close" />
          ) : null}
        </View>
      </View>
      {focus == true ? (
        <SearchVegetables
          navigation={props.navigation}
          productsFiltered={productsFiltered}
        />
      ) : (
        <View style={{ marginTop: 1 }}>
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <VegetableList
                navigation={props.navigation}
                key={item.id}
                item={item}
              />
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      )}
    </ScrollView>
  );
};
export default VegetableContainer;

const styles = StyleSheet.create({
  Searched: {
    margin: 12,
  },
});
