import { StatusBar } from "expo-status-bar";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { StyleSheet, Text, View, LogBox } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";
import { createStackNavigator } from "@react-navigation/stack";
import BottomTab from "./component/Navigators/BottomTab";
import TopTab from "./component/Navigators/TopTab";
import Login from "./screens/Login";
import SignIn from "./screens/SignIn";
import VegetableForm from "./screens/PostForm/VegetablePostForm";
import ReportForm from "./screens/PostForm/ReportForm";
import MaterialForm from "./screens/PostForm/MaterialPostForm";
import UpcomingForm from "./screens/PostForm/UpcomingProduct";
import UpdateProfileScreen from "./screens/UpdateProfileScreen";
import PasswordChangeForm from "./screens/PasswordUpdate";
// import FeedbackForm from "./screens/PostForm/FeedBackForm";
import MainDetailScreen from "./MainScreen/DestailScreen";
import VegetableDetailScreen from "./VegetableScreen/VegetableDetailScreen";
import MaterialDetailScreen from "./MaterialScreen/MaterialDetailScreen";
import UpcomingProductDetailScreen from "./UpcomingScreen/UpcomingProductDetailScreen";
import MyPostDetailScreen from "./MyPost/PostDetailScreen";
import EditPostScreen from "./MyPost/MyPostEdit";
import FeedbackScreen from "./screens/PostForm/NewFeedBackForm";
import AboutUsScreen from "./screens/AboutUs";
import UpcomingPostForm from "./screens/PostForm/UpcomingProduct";

const Stack = createStackNavigator();

LogBox.ignoreAllLogs(true);

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="HomeBottom"
            component={BottomTab}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="MainDetailScreen"
            component={MainDetailScreen}
            options={{ headerShown: true }}
          />
          <Stack.Screen
            name="VegetableDetails"
            component={VegetableDetailScreen}
          />
          <Stack.Screen
            name="MaterialDetails"
            component={MaterialDetailScreen}
          />
          <Stack.Screen
            name="UpcomingDetails"
            component={UpcomingProductDetailScreen}
          />
           <Stack.Screen
            name="My Post Details"
            component={MyPostDetailScreen}
          />
           <Stack.Screen
            name="My Edit Post"
            component={EditPostScreen}
          />
          <Stack.Screen
            name="TopTabNavigator"
            component={TopTab}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="SignIn"
            component={SignIn}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="VegetablePostForm"
            component={VegetableForm}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="MaterialPostForm"
            component={MaterialForm}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="UpcomingForm"
            component={UpcomingPostForm}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="UpdateProfile"
            component={UpdateProfileScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="PasswordUpdate"
            component={PasswordChangeForm}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="FeedBack"
            component={FeedbackScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Report"
            component={ReportForm}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AboutUs"
            component={AboutUsScreen}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
