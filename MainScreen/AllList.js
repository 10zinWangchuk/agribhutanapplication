import React from "react";
import { TouchableOpacity, View, Dimensions, StyleSheet } from "react-native";
import AllCard from './AllCard'

const AllListContainer = (props) => {
  const { item } = props;
  return (
    <TouchableOpacity
      style={styles.Container}
      onPress={() =>
        props.navigation.navigate("MainDetailScreen", { item: item })
      }
    >
      <View style={styles.innerContainer}>
        <AllCard {...item} />
      </View>
    </TouchableOpacity>
  );
};
export default AllListContainer;
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    width: "50%",
  },
  innerContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
    backgroundColor: "#ccc",
  },
});
