import React, { useState, useEffect } from "react";
import {
  Image,
  View,
  StyleSheet,
  Text,
  ScrollView,
  Button,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useNavigation } from "@react-navigation/native";

const windowWidth = Dimensions.get("window").width;

function MainDetailScreen(props) {
  const navigation = useNavigation();
  const [item, setItem] = useState(props.route.params.item);

  const handleReport = () => {
    navigation.navigate("Report");
  };
  return (
    <ScrollView contentContainerStyle={styles.rootScreen}>
      <View style={styles.container}>
        <View>
          <Image
            source={{
              uri: item.image
                ? item.image
                : "https://cdn.pixabay.com/photo/2016/12/05/10/07/dish-1883501_1280.png",
            }}
            style={styles.image}
          />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{item.name}</Text>
          <Text style={styles.description}>Price: {item.price}</Text>
          <Text style={styles.description}>Location: {item.location}</Text>
          <Text style={styles.description}>Quantity: {item.TotalQuantity}</Text>
          <Text style={styles.description}>Contact: {item.contact}</Text>
          <Text style={styles.description}>AvailDate: {item.AvailDate}</Text>
          <Text style={styles.description}>
            Description: {item.description}
          </Text>
        </View>
        <TouchableOpacity style={styles.report} onPress={handleReport}>
          <Ionicons name="md-warning-outline" size={25} color="black" />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

export default MainDetailScreen;

const styles = StyleSheet.create({
  rootScreen: {
    flexGrow: 1,
    justifyContent: "center",
  },
  container: {
    backgroundColor: "#cc",
    overflow: "hidden",
    margin: 12,
    borderRadius: 12,
    overflow: Platform.OS == "android" ? "hidden" : "visible",
    elevation: 12,
    shadowColor: "black",
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 2,
  },
  image: {
    width: "100%",
    height: 200,
  },
  textContainer: {
    marginHorizontal: 12,
    padding: 6,
  },
  title: {
    fontWeight: "bold",
    margin: 9,
    textAlign: "center",
    fontSize: windowWidth > 360 ? 24 : 20, // Adjust font size based on device width
    color: "black",
  },
  description: {
    backgroundColor: "#ccc",
    alignItems: "center",
    borderRadius: 12,
    margin: 6,
    padding: 12,
    elevation: 6,
  },
  report: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    marginHorizontal: 12,
    marginVertical: 8,
  },
});
