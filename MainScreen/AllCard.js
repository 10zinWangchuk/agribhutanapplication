import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  Text,
  Button,
} from "react-native";
var { width } = Dimensions.get("window");
const AllCard = (props) => {
  const { image, name, price } = props;
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        resizeMode="contain"
        source={{
          uri: image
            ? image
            : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
        }}
      />
      <View style={styles.card}>
        <Text style={styles.title}>
          {name.length > 15 ? name.substring(0, 15 - 3) + "..." : name}
        </Text>
        <Text style={styles.price}>Nu:{price}</Text>
      </View>
    </View>
  );
};
export default AllCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width / 2 - 20,
    height: width / 1.7,
    padding: 10,
    borderRadius: 10,
    marginTop: 20,
    marginHorizontal: '4%',
    alignItems: "center",
    elevation: 8,
    backgroundColor: "white",
  },
  image: {
    width: 140,
    height: 140,
    borderRadius: 12
  },

  title: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
  },
  price: {
    fontSize: 20,
    color: "orange",
    marginTop: 8,
    marginBottom: 5
  },
});
