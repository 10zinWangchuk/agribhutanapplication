import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { StyleSheet, View } from "react-native";
import VegetableContainer from "../../MainScreen/VegetableContainer";
import VegetableScreen from "../../VegetableScreen/VegetableScreen";
import MaterialContainer from "../../MaterialScreen/MaterialScreen";
import UpcomingProductContainer from "../../UpcomingScreen/UpcomingProductScreen";
const Tab = createMaterialTopTabNavigator();

function TopTab() {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: { backgroundColor: "white" },
        tabBarInactiveTintColor: "black",
        tabBarActiveTintColor: "#ccc",
        tabBarStyle: {
          backgroundColor: "#2DB064",
          paddingVertical: 2,
        },
        tabBarLabelStyle: { ...styles.tabLabel, marginHorizontal: 2},
      }}
    >
      <Tab.Screen name="Home" component={VegetableContainer} />
      <Tab.Screen name="Vegetable" component={VegetableScreen} />
      <Tab.Screen name="materials" component={MaterialContainer} />
      <Tab.Screen name="Upcoming" component={UpcomingProductContainer} />
    </Tab.Navigator>
  );
}
export default TopTab;

const styles = StyleSheet.create({
  tabLabel: {
    fontSize: 12, // Decrease the font size to your desired value
  },
});
