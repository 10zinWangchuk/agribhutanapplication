import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Dimensions } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { Octicons } from "@expo/vector-icons";
import { useSafeAreaInsets } from "react-native-safe-area-context"; // Import the hook
import ProfileScreen from "../../screens/ProfileScreen";
import TopTab from "./TopTab";
import PostComponentScreen from "../../screens/PostForm/PostScreen";
import MyPostContainer from "../../MyPost/MyPostScreen";

const Tab = createBottomTabNavigator();
const windowWidth = Dimensions.get("window").width;

function BottomTab() {
  const insets = useSafeAreaInsets(); // Get the safe area insets

  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({}) => ({
        tabBarHideOnKeyboard: false,
        tabBarActiveTintColor: "white",
        tabBarShowLabel: true,
        tabBarStyle: {
          backgroundColor: "#2DB064",
          paddingVertical: 8,
          width: windowWidth,
          paddingTop: 12 + insets.bottom, // Add the safe area bottom insets to the padding
          paddingBottom: insets.bottom, // Add the safe area bottom insets to the padding
          borderTopLeftRadius: 20, // Add border radius to the top left
          borderTopRightRadius: 20, // Add border radius to the top right
          height: 60, // Increase the height of the bottom tab
        },
      })}
    >
      <Tab.Screen
        name="Home"
        component={TopTab}
        options={{
          headerShown: false,
          tabBarIcon: () => (
            <Ionicons
              name="home-outline"
              style={{ position: "relative" }}
              color="#fff"
              size={30}
            />
          ),
        }}
      />

      <Tab.Screen
        name="My-Post"
        component={MyPostContainer}
        options={{
          headerShown: true,
          tabBarIcon: () => (
            <AntDesign
              name="contacts"
              style={{ position: "relative" }}
              color="#fff"
              size={30}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Post"
        component={PostComponentScreen}
        options={{
          headerShown: true,
          tabBarIcon: () => (
            <Octicons
              name="diff-added"
              style={{ position: "relative" }}
              color="#fff"
              size={30}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerShown: false,
          tabBarIcon: () => (
            <Ionicons
              name="ios-person-circle-outline"
              style={{ position: "relative" }}
              color="#fff"
              size={32}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTab;
