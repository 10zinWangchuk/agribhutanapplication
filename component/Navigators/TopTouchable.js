import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import VegetableScreen from "../../screens/VegetableScreen";
import MaterialScreen from "../../screens/MaterialScreen";
import UpcomingProductScreen from "../../screens/UpcomingProductScreen";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import ProductContainer from "../../screens/Products/ProductContainer";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";



import Search from "../search";
const Tab = createMaterialTopTabNavigator();

function TopTab1() {
  return (
    <>
      <View style={styles.Search}>
        <Search />
      </View>
      <View style={styles.allPressabel}>
        <TouchableOpacity style={styles.MyTouchable} onPress={ProductContainer}>
            <Ionicons name="mail" size={24}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.MyTouchable} >
            <Ionicons name="mail" size={24}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.MyTouchable} >
            <Ionicons name="mail" size={24}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.MyTouchable} >
            <Ionicons name="mail" size={24}/>
        </TouchableOpacity>
      </View>
       {/* <Tab.Screen name="Home" component={ProductContainer} />
        <Tab.Screen name="Vegetable" component={VegetableScreen} />
        <Tab.Screen name="materials" component={MaterialScreen} />
        <Tab.Screen name="Upcoming" component={UpcomingProductScreen} /> */}
    </>
  );
}
export default TopTab1;

const styles = StyleSheet.create({
  tabLabel: {
    fontSize: 12, // Decrease the font size to your desired value
  },
  allPressabel:{
    flexDirection: 'row'
  },
  Search:{
    // margin: '3%'
  },
  MyTouchable: {
    borderRadius: 30,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10, 
    width: 60,
    height: 60,
    marginHorizontal: '4%'
  }
});
