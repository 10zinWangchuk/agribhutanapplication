import React from "react";
import { StyleSheet, View, Dimensions, Image, Text } from "react-native";

var { width } = Dimensions.get("window");

const MaterialCard = (props) => {
  const { image, name, price, location, TotalQuantity, contact } = props;
  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.image}
          resizeMode="contain"
          source={{
            uri: image
              ? image
              : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
          }}
        />
      </View>
      <View>
        <Text style={styles.title}>
          {name.length > 15 ? name.substring(0, 15 - 3) + "..." : name}
        </Text>
        <Text style={styles.price}>Nu:{price}</Text>
        <Text style={styles.description}>
          Location:
          {location.length > 15
            ? location.substring(0, 15 - 3) + "..."
            : location}
        </Text>
        <Text style={styles.description}>Quantity: {TotalQuantity}</Text>
        <Text style={styles.description}>Contact: {contact}</Text>
      </View>
    </View>
  );
};
export default MaterialCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    padding: 8,
    borderRadius: 10,
    marginVertical: "2%",
    alignItems: "center",
    elevation: 8,
    backgroundColor: "white",
    width: "95%",
  },
  image: {
    width: 130,
    height: 130,
    borderRadius: 12,
    margin: 8,
  },

  title: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
  },
  price: {
    fontSize: 15,
    marginTop: 8,
    marginBottom: 5,
  },
});
