import React from "react";
import { TouchableOpacity, View, Dimensions, StyleSheet } from "react-native";
import MaterialCard from "./MaterialCard";


const MaterialList = (props) => {
  const { item } = props;
  return (
    <TouchableOpacity
      style={styles.Container}
      onPress={() =>
        props.navigation.navigate("MaterialDetails", { item: item })
      }
    >
      <View style={styles.innerContainer}>
        <MaterialCard {...item} />
      </View>
    </TouchableOpacity>
  );
};
export default MaterialList;
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    width: "100%",
  },
  innerContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    // width: "100%",
    backgroundColor: "#ccc",
  },
});
