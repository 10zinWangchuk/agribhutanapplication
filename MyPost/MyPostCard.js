import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  Text,
  Button,
} from "react-native";
var { width } = Dimensions.get("window");
const PostCard = (props) => {
  const { image, name, price } = props;
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        resizeMode="contain"
        source={{
          uri: image
            ? image
            : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
        }}
      />
    </View>
  );
};
export default PostCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width / 2 - 20,
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    alignItems: "center",
    elevation: 8,
    backgroundColor: "white",
  },
  image: {
    width: 140,
    height: 140,
    borderRadius: 12,
  },
});
