import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  ScrollView,
  TextInput,
  StyleSheet,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import SearchedProduct from "./MyPostSearch";
import MyPostList from "./MyPostList";

const Vdata = require("../assets/data/Vegetable.json");
const Mdata = require("../assets/data/Material.json");
const Udata = require("../assets/data/Upcoming.json");

const MyPostContainer = (props) => {
  const combinedData = [...Vdata, ...Mdata, ...Udata];
  const [items, setItems] = useState([]);
  const [productsFiltered, setProductsFiltered] = useState([]);
  const [focus, setFocus] = useState();

  useEffect(() => {
    setItems(combinedData);
    setProductsFiltered(combinedData);

    return () => {
      setItems([]);
      setProductsFiltered([]);
      setFocus();
    };
  }, []);
  const searchProduct = (text) => {
    setProductsFiltered(
      items.filter((i) => i.name.toLowerCase().includes(text.toLowerCase()))
    );
  };
  const openList = () => {
    setFocus(true);
  };
  const onBlur = () => {
    setFocus(false);
  };

  return (
    <ScrollView>
      <View style={styles.Searched}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            borderRadius: 10,
            backgroundColor: "#ccc",
            padding: 8,
          }}
        >
          <Icon name="search" size={20} style={{ marginLeft: 10 }} />
          <TextInput
            style={{ flex: 1, marginLeft: 10 }}
            placeholder="Search"
            onFocus={openList}
            onChangeText={(text) => searchProduct(text)}
          />
          {focus == true ? (
            <Icon size={20} onPress={onBlur} name="close" />
          ) : null}
        </View>
      </View>
      {focus == true ? (
        <SearchedProduct
          navigation={props.navigation}
          productsFiltered={productsFiltered}
        />
      ) : (
        <View style={{ marginTop: 1 }}>
          <FlatList
            numColumns={2}
            data={combinedData}
            renderItem={({ item }) => (
              <MyPostList
                navigation={props.navigation}
                key={item.id}
                item={item}
              />
            )}
            keyExtractor={(item) => item.id}
          />
        </View>
      )}
    </ScrollView>
  );
};
export default MyPostContainer;

const styles = StyleSheet.create({
  Searched: {
    margin: 12,
  },
});
