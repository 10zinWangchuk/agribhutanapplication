import React from "react";
import { TouchableOpacity, View, Dimensions, StyleSheet } from "react-native";
import MyPostCard from './MyPostCard';
const MyPostList = (props) => {
  const { item } = props;
  return (
    <TouchableOpacity
      style={styles.Container}
      onPress={() =>
        props.navigation.navigate("My Post Details", { item: item })
      }
    >
      <View style={styles.innerContainer}>
        <MyPostCard {...item} />
      </View>
    </TouchableOpacity>
  );
};
export default MyPostList;
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    width: "50%",
  },
  innerContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
    backgroundColor: "#ccc",
  },
});
