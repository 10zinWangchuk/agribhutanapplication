import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import NumericInput from "react-native-numeric-input";

function EditPostScreen(props) {
  const [name, setName] = useState(props.route.params.item.name);
  const [price, setPrice] = useState(props.route.params.item.price);
  const [location, setLocation] = useState(props.route.params.item.location);
  const [quantity, setQuantity] = useState(
    props.route.params.item.TotalQuantity
  );
  const [contact, setContact] = useState(props.route.params.item.contact);
  const [availDate, setAvailDate] = useState(props.route.params.item.AvailDate);
  const [description, setDescription] = useState(
    props.route.params.item.description
  );
  const [image, setImage] = useState(props.route.params.item.image);

  const handleSave = () => {
    // Handle saving the edited post data
  };
  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.canceled) {
      setImage(result.assets[0].uri);
    }
  };
  console.log(contact);
  return (
    <View style={styles.container}>
      <ScrollView>
        <TouchableOpacity style={styles.imageContainer}>
          <Image source={{ uri: image }} style={styles.image} />
          <Ionicons
            style={styles.editIcon}
            name="camera-outline"
            size={38}
            onPress={pickImage}
          />
        </TouchableOpacity>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Name</Text>
          <TextInput style={styles.input} value={name} onChangeText={setName} />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Price</Text>
          <TextInput
            style={styles.input}
            value={price}
            onChangeText={setPrice}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Location</Text>
          <TextInput
            style={styles.input}
            value={location}
            onChangeText={setLocation}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Quantity</Text>
          <TextInput
            style={styles.input}
            value={quantity}
            onChangeText={setQuantity}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Contact</Text>
          <NumericInput
            inputStyle={styles.input}
            value={contact}
            onChange={setContact}
            totalWidth={328}
            totalHeight={43}
            rounded
            textColor="#000"
            iconStyle={{ color: "white" }}
            rightButtonBackgroundColor="white"
            leftButtonBackgroundColor="white"
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>AvailDate</Text>
          <TextInput
            style={styles.input}
            value={availDate}
            onChangeText={setAvailDate}
          />
        </View>
        <View style={styles.inputContainer}>
          <Text style={styles.label}>Description</Text>
          <TextInput
            style={styles.input}
            value={description}
            onChangeText={setDescription}
            multiline
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={handleSave}>
          <Text style={styles.buttonText}>Save</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

export default EditPostScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
    backgroundColor: "#cc",
  },
  imageContainer: {
    alignItems: "center",
    marginBottom: 16,
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: "gray",
    marginTop: "3%",
  },
  addImageText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#2DB064",
  },
  inputContainer: {
    marginBottom: 10,
    backgroundColor: "#ccc",
    borderRadius: 12,
  },
  label: {
    fontWeight: "bold",
    marginBottom: 4,
    paddingHorizontal: 12,
  },
  input: {
    backgroundColor: "#fff",
    padding: 10,
    borderRadius: 5,
    margin: 4,
  },
  button: {
    backgroundColor: "#2DB064",
    padding: 12,
    alignItems: "center",
    borderRadius: 5,
    marginTop: 16,
  },
  buttonText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16,
  },
  editIcon: {
    marginTop: -24,
    marginLeft: "30%",
  },
});
